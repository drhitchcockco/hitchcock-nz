---
title: A first post
date: '2020-11-06'
tags:
  - Blogging
---

Nothing super interesting. I've just got the local dev environment all set up and hosted on Netlify. I want my next post to be about compassion in the tech community.

See you then,<br>
✌️
